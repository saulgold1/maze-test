# maze-test

## Setup
Using Python 3.6 with Numpy package

## Run
Run the mouse.py script

the Maze can be inputted as an nxm numpy array

Where:

0 is an open path

1 a wall

2 is the starting position

### eg
    maze = np.array(((1, 1, 1, 1, 1, 1),
                     (1, 0, 0, 0, 0, 1),
                     (1, 1, 1, 2, 1, 1),
                     (1, 0, 0, 0, 0, 1),
                     (1, 1, 1, 1, 1, 1)))

## Details
The Mouse object can deal with mazes with multiple junctions.
If the maze is impossible the program will say so.