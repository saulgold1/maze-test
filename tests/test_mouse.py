from mouse import Mouse
import numpy as np
# Tests configured with Pytest
# https://docs.pytest.org/en/latest/

# Inm
maze = np.array(((1, 0, 1, 1, 1, 1),
                 (1, 0, 1, 0, 0, 1),
                 (1, 0, 1, 0, 1, 1),
                 (1, 0, 0, 0, 2, 1),
                 (1, 1, 1, 1, 1, 1)))
coord = (3,4)

def test_mouse_init():
    mouse = Mouse(maze)
    assert mouse is not None
    assert mouse.location is not None
    assert mouse.live_maze is not None
    assert mouse.maze[mouse.location[0],mouse.location[1]] == 2



def test_get_possible_coords():
    mouse = Mouse(maze)
    potential_coords = mouse.get_possible_coords(coord)
    assert potential_coords is not None
    assert len(potential_coords) == 1
    assert potential_coords ==[(3,3)]


def test_mouse_move():
    pass