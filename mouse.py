import numpy as np
'''
Class containing information about the mouse
'''
class Mouse:
    def __init__(self, maze):
        start_idx = np.argmax(maze)
        start_location = np.unravel_index(start_idx, maze.shape)

        self.maze = maze
        self.location = start_location
        self.previous_loc = start_location
        self.history = []
        self.history.append(start_location)
        self.live_maze = maze.copy()
        self.live_maze[start_location[0],start_location[1]] = 3
        self.is_finished = False
        self.previous_junctions = []
        self.junc_count = 1
        pass

    '''
    movement function which finds all the possible movement directions,
    and saves any junctions, in case a route does not work out
    '''
    def move(self):
        self.see_if_finished(self.location)

        #Check if the mouse has finished the maze
        if not self.is_finished:

            # Get all possible movement coordinates
            coords = self.get_possible_coords(self.location)

            # Removed coordinates if they have been used before
            new_coords = self.remove_history(coords,self.history)

            # If only one possible direction
            if len(new_coords) == 1:
                self.set_location(new_coords[0])
                self.history.append(self.location)

            # If multiple directions
            elif len(new_coords) > 1:

                # Record a junction in case the mouse needs to go back
                self.previous_junctions.append((self.location, len(new_coords)))

                # Find the possible coordinate with the most options for future movement
                options = []
                for c in new_coords:
                    options.append(len(self.get_possible_coords(c)))
                self.set_location(new_coords[np.argmax(options)])
                self.history.append(self.location)

            # If the mouse has reached a dead end
            else:
                # If the junction has already been tried
                if self.junc_count == self.previous_junctions[0][1]:
                    # Removed junction
                    self.previous_junctions.pop()
                    self.junc_count = 0

                # If there are no previous junctions then the maze has no exit
                if len(self.previous_junctions) == 0:
                    print('Maze has no exit!')
                    self.is_finished = True
                    return -1
                # Revert mouse location to the previous junctio
                self.set_location(self.previous_junctions[-1][0])
                self.junc_count = self.junc_count+1

            #update live maze for visualisation
            self.live_maze[self.location[0],self.location[1]] = 3
            self.live_maze[self.previous_loc[0],self.previous_loc[1]] = self.maze[self.previous_loc[0],self.previous_loc[1]]

    '''
    Set mouse location, while also saving the previous location
    '''
    def set_location(self, coord):
        self.previous_loc = self.location
        self.location = coord

    '''
    Search all around coordinate to see if mouse has reached the end of the maze
    '''
    def see_if_finished(self, c):

        if c[0] >= self.maze.shape[0]:
            self.is_finished = True
            return 1
        if c[1] >= self.maze.shape[1]:
            self.is_finished = True
            return 1

        if c[0] <=0:
            self.is_finished = True
            return 1

        if c[1] <=0 :
            self.is_finished = True
            return 1


    '''
    Remove coordinate if it is contained in the history
    '''
    def remove_history(self,coords, history):
        new_coords = [i for i in coords if i not in history]
        return new_coords

    '''
    Get possible coordinates to move to
    '''
    def get_possible_coords(self,current_loc):
        current_locs = (current_loc[0]+1, current_loc[1]), \
                       (current_loc[0]-1,current_loc[1]), \
                       (current_loc[0], current_loc[1]+1), \
                       (current_loc[0], current_loc[1]-1)
        potential_locs = []
        for loc in current_locs:
            if loc[0]>=0 and loc[0]<self.maze.shape[0]:
                if loc[1]>=0 and loc[1] <self.maze.shape[1]:
                    if self.maze[loc[0], loc[1]] == 0 or self.maze[loc[0], loc[1]] == 2 :
                        potential_locs.append(loc)


        return potential_locs



if __name__ == '__main__':
    # Origional maze
    # maze = np.array(((1, 0, 1, 1, 1, 1),
    #                  (1, 0, 1, 0, 0, 1),
    #                  (1, 0, 1, 0, 1, 1),
    #                  (1, 0, 0, 0, 2, 1),
    #                  (1, 1, 1, 1, 1, 1)))

    # Test maze with multiple junctions
    maze = np.array(((1, 0, 1, 1, 1, 1),
                     (1, 0, 0, 0, 0, 1),
                     (1, 1, 1, 2, 1, 1),
                     (1, 0, 0, 0, 0, 1),
                     (1, 1, 1, 1, 1, 1)))

    # Test maze with no exit
    # maze = np.array(((1, 1, 1, 1, 1, 1),
    #                  (1, 0, 0, 0, 0, 1),
    #                  (1, 1, 1, 2, 1, 1),
    #                  (1, 0, 0, 0, 0, 1),
    #                  (1, 1, 1, 1, 1, 1)))

    mouse = Mouse(maze)
    while True:
        if mouse.is_finished:
            break
        mouse.move()
        print(mouse.live_maze)
        print('')


    print('Finished!')
    pass
